<?php

namespace Drupal\nbox\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Nbox type entities.
 */
interface NboxTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
