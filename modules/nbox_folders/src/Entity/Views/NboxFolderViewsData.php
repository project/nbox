<?php

namespace Drupal\nbox_folders\Entity\Views;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Nbox folder entities.
 */
class NboxFolderViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();
    return $data;
  }

}
