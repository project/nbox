<?php

namespace Drupal\nbox_ui\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Nbox folder entities.
 *
 * @ingroup nbox_folders
 */
class NboxFolderDeleteForm extends ContentEntityDeleteForm {

}
